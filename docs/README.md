[returnlogic - v1.0.0](README.md)

# returnlogic - v1.0.0

## Index

### Interfaces

* [IProcessReturnPlugIn](interfaces/iprocessreturnplugin.md)
* [Return](interfaces/return.md)
* [ReturnLine](interfaces/returnline.md)

### Type aliases

* [ProcessReturnFunction](README.md#processreturnfunction)

## Type aliases

###  ProcessReturnFunction

Ƭ **ProcessReturnFunction**: *function*

#### Type declaration:

▸ (`returnData`: [Return](interfaces/return.md)): *number*

**Parameters:**

Name | Type |
------ | ------ |
`returnData` | [Return](interfaces/return.md) |
