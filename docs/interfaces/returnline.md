[returnlogic - v1.0.0](../README.md) › [ReturnLine](returnline.md)

# Interface: ReturnLine

**`copyright`** 2020 ExtendApps, Inc.

**`author`** Darren Hill darren@extendapps.com

## Hierarchy

* **ReturnLine**

## Index

### Properties

* [delivered_update_date](returnline.md#optional-delivered_update_date)
* [disposition](returnline.md#disposition)
* [disposition_comment](returnline.md#optional-disposition_comment)
* [in_transit_update_date](returnline.md#optional-in_transit_update_date)
* [item_received](returnline.md#item_received)
* [order_create_date](returnline.md#order_create_date)
* [order_line_price](returnline.md#order_line_price)
* [order_order_number](returnline.md#order_order_number)
* [parent_productid_source](returnline.md#parent_productid_source)
* [product_name](returnline.md#product_name)
* [product_variant_name](returnline.md#product_variant_name)
* [productid_source](returnline.md#productid_source)
* [received_date](returnline.md#received_date)
* [restock_processed](returnline.md#restock_processed)
* [return_comment](returnline.md#return_comment)
* [return_reason](returnline.md#return_reason)
* [return_reason_code](returnline.md#return_reason_code)
* [return_type](returnline.md#return_type)
* [rl_rma_id](returnline.md#rl_rma_id)
* [rma_create_date](returnline.md#rma_create_date)
* [rma_email_id](returnline.md#rma_email_id)
* [sku](returnline.md#sku)
* [totalrows](returnline.md#totalrows)
* [workflow_status](returnline.md#workflow_status)

## Properties

### `Optional` delivered_update_date

• **delivered_update_date**? : *any*

___

###  disposition

• **disposition**: *"Ready for Resale" | "Liquidate" | "Damaged" | "Incomplete" | "Additional Units" | "Unassigned"*

___

### `Optional` disposition_comment

• **disposition_comment**? : *any*

___

### `Optional` in_transit_update_date

• **in_transit_update_date**? : *any*

___

###  item_received

• **item_received**: *boolean*

___

###  order_create_date

• **order_create_date**: *Date*

___

###  order_line_price

• **order_line_price**: *number*

___

###  order_order_number

• **order_order_number**: *string*

___

###  parent_productid_source

• **parent_productid_source**: *string*

___

###  product_name

• **product_name**: *string*

___

###  product_variant_name

• **product_variant_name**: *string*

___

###  productid_source

• **productid_source**: *string*

___

###  received_date

• **received_date**: *Date*

___

###  restock_processed

• **restock_processed**: *boolean*

___

###  return_comment

• **return_comment**: *string*

___

###  return_reason

• **return_reason**: *string*

___

###  return_reason_code

• **return_reason_code**: *string*

___

###  return_type

• **return_type**: *"Store Credit" | "Exchange" | "Refund" | "Warranty"*

___

###  rl_rma_id

• **rl_rma_id**: *number*

___

###  rma_create_date

• **rma_create_date**: *Date*

___

###  rma_email_id

• **rma_email_id**: *string*

___

###  sku

• **sku**: *string*

___

###  totalrows

• **totalrows**: *number*

___

###  workflow_status

• **workflow_status**: *"Requested" | "Authorized" | "Cancelled" | "In Transit" | "Delivered" | "Received" | "Processed" | "Completed" | "Rejected"*
