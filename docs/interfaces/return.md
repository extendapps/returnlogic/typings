[returnlogic - v1.0.0](../README.md) › [Return](return.md)

# Interface: Return

## Hierarchy

* **Return**

## Index

### Properties

* [data](return.md#data)
* [fileName](return.md#filename)
* [name](return.md#name)
* [type](return.md#type)

## Properties

###  data

• **data**: *[ReturnLine](returnline.md)[]*

___

###  fileName

• **fileName**: *string*

___

###  name

• **name**: *string*

___

###  type

• **type**: *string*
