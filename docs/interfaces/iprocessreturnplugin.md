[returnlogic - v1.0.0](../README.md) › [IProcessReturnPlugIn](iprocessreturnplugin.md)

# Interface: IProcessReturnPlugIn

## Hierarchy

* **IProcessReturnPlugIn**

## Index

### Properties

* [processReturn](iprocessreturnplugin.md#processreturn)

## Properties

###  processReturn

• **processReturn**: *[ProcessReturnFunction](../README.md#processreturnfunction)*
