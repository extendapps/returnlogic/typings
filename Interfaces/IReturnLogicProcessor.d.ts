import { Return } from '../Objects/Return';
export interface IProcessReturnPlugIn {
    processReturn: ProcessReturnFunction;
}
export declare type ProcessReturnFunction = (returnData: Return) => number;
