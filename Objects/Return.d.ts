/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import { ReturnLine } from './ReturnLine';
export interface Return {
    data: ReturnLine[];
    name: string;
    type: string;
    fileName: string;
}
