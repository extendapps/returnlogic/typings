/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface ReturnLine {
    totalrows: number;
    rl_rma_id: number;
    rma_email_id: string;
    order_order_number: string;
    order_create_date: Date;
    rma_create_date: Date;
    return_type: 'Store Credit' | 'Exchange' | 'Refund' | 'Warranty';
    return_reason: string;
    return_reason_code: string;
    return_comment: string;
    order_line_price: number;
    productid_source: string;
    parent_productid_source: string;
    sku: string;
    product_name: string;
    product_variant_name: string;
    workflow_status: 'Requested' | 'Authorized' | 'Cancelled' | 'In Transit' | 'Delivered' | 'Received' | 'Processed' | 'Completed' | 'Rejected';
    in_transit_update_date?: any;
    delivered_update_date?: any;
    received_date: Date;
    item_received: boolean;
    disposition: 'Ready for Resale' | 'Liquidate' | 'Damaged' | 'Incomplete' | 'Additional Units' | 'Unassigned';
    disposition_comment?: any;
    restock_processed: boolean;
}
